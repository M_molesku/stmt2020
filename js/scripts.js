$(document).ready(function() {

	// Setup the player to autoplay the next track
    var a = audiojs.createAll({
      trackEnded: function() {
        var next = $('ol li.playing').next();
        if (!next.length) next = $('ol li').first();
        next.addClass('playing').siblings().removeClass('playing');
        audio.load($('a', next).attr('data-src'));
        audio.play();
      }
    });
    
    // Load in the first track + change volume
    var audio = a[0];
    var music = $('#jcp-volume').val();
    $('#jcp-volume').on('change', function() {
    	var x = this.value;
    	audio.setVolume(x/10);
    });
    first = $('ol a').attr('data-src');
    $('ol li').first().addClass('playing');
    audio.load(first);

    // Load in a track on click
    $('ol li').click(function(e) {
      e.preventDefault();
      $(this).addClass('playing').siblings().removeClass('playing');
      audio.load($('a', this).attr('data-src'));
      audio.play();
    });

    // Keyboard shortcuts
    $(document).keydown(function(e) {
      var unicode = e.charCode ? e.charCode : e.keyCode;
         // right arrow
      if (unicode == 39) {
        var next = $('li.playing').next();
        if (!next.length) next = $('ol li').first();
        next.click();
        // back arrow
      } else if (unicode == 37) {
        var prev = $('li.playing').prev();
        if (!prev.length) prev = $('ol li').last();
        prev.click();
        // spacebar
      } else if (unicode == 32) {
        audio.playPause();
      }
      var $text = $('li.playing a').text();
        $('.track__name').text($text);
    })

    // Track name change
    $('ol li a').on('click', function() {

    	var $text = $(this).text();
    	$('.track__name').text($text);

    });

    // Image change
    $('ol li a').on('click', function() {

    	var $data = $(this).attr('data-number');
    	var $src  = $('.player__img').attr('src');
    	$('.player__img').attr('src', "https://gitlab.com/M_molesku/stmt2020/-/raw/b0cad855a7abc520ceccca2d77730d9a23cb3d1a/img/" + $data + ".jpg");

    });

    // Song scroll
	document.addEventListener('wheel', (e) => {
	    document.getElementById('customScroll').scrollTop += e.deltaY;
	});

	// Stems open
	$('ol .composition').on('click', function() {
		$('.stem').fadeTo('fast', 0).css('display', 'none');
		$('.stem1').fadeTo('fast', 1);
	});

	$('.stem1').on('click', function() {
		var $stem1 = $(this).attr('data-picture');
    	$('.player__img').attr('src', "https://gitlab.com/M_molesku/stmt2020/-/raw/b0cad855a7abc520ceccca2d77730d9a23cb3d1a/img/" + $stem1);
	});

	$('ol .composition2').on('click', function() {
		$('.stem').fadeTo('fast', 0).css('display', 'none');
		$('.stem2').fadeTo('fast', 1);
	});

	$('.stem2').on('click', function() {
		var $stem1 = $(this).attr('data-picture');
    	$('.player__img').attr('src', "https://gitlab.com/M_molesku/stmt2020/-/raw/b0cad855a7abc520ceccca2d77730d9a23cb3d1a/img/" + $stem1);
	});

	$('ol .composition3').on('click', function() {
		$('.stem').fadeTo('fast', 0).css('display', 'none');
		$('.stem3').fadeTo('fast', 1);
	});

	$('.stem3').on('click', function() {
		var $stem1 = $(this).attr('data-picture');
    	$('.player__img').attr('src', "https://gitlab.com/M_molesku/stmt2020/-/raw/b0cad855a7abc520ceccca2d77730d9a23cb3d1a/img/" + $stem1);
	});

	$('ol .composition4').on('click', function() {
		$('.stem').fadeTo('fast', 0).css('display', 'none');
		$('.stem4').fadeTo('fast', 1);
	});

	// Buttons play track
    $(document).ready(function() {
    	$('.audiojs .next__song').on('click', function() {
    		var next = $('li.playing').next();
	        if (!next.length) next = $('ol li').first();
	        next.click();
	        var $text = $('li.playing a').text();
	        $('.track__name').text($text);
    	});
    	$('.audiojs .previos__song').on('click', function() {
    		var prev = $('li.playing').prev();
	        if (!prev.length) prev = $('ol li').last();
	        prev.click();
	        var $text = $('li.playing a').text();
	        $('.track__name').text($text);
    	});
    });

});